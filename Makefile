CC=gcc
CFLAGS=-g -Wall

readframe: readframe.o
	$(CC) $< $(LDFLAGS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f *.o readframe

.PHONY: clean
