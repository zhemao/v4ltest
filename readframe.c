#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

int setformat(int fd, int pixelformat)
{
	struct v4l2_format format;

	format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (ioctl(fd, VIDIOC_G_FMT, &format) < 0) {
		perror("VIDIOC_G_FMT");
		return -1;
	}

	if (format.fmt.pix.pixelformat == pixelformat)
		return 0;
	
	format.fmt.pix.pixelformat = pixelformat;

	if (ioctl(fd, VIDIOC_S_FMT, &format)) {
		perror("VIDIOC_S_FMT");
		return -1;
	}

	return 0;
}

int reqbuffer(int fd)
{
	struct v4l2_requestbuffers reqbuf;
	
	memset(&reqbuf, 0, sizeof(reqbuf));
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	reqbuf.memory = V4L2_MEMORY_MMAP;
	reqbuf.count = 1;

	if (ioctl(fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
		perror("VIDIOC_REQBUFS");
		return -1;
	}
	
	return reqbuf.count;
}

int readframe(int fd, void **addr)
{
	struct v4l2_buffer buf;

	memset(&buf, 0, sizeof(buf));
	buf.index = 0;
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (ioctl(fd, VIDIOC_QUERYBUF, &buf) == -1) {
		perror("VIDIOC_QUERYBUF");
		return -1;
	}

	*addr = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, 
			MAP_SHARED, fd, buf.m.offset);

	if (*addr == MAP_FAILED) {
		*addr = NULL;
		return -1;
	}

	if (ioctl(fd, VIDIOC_QBUF, &buf) == -1) {
		perror("VIDIOC_QBUF");
		goto error;
	}

	if (ioctl(fd, VIDIOC_STREAMON, &buf.type) == -1) {
		perror("VIDIOC_STREAMON");
		goto error;
	}

	if (ioctl(fd, VIDIOC_DQBUF, &buf) == -1) {
		perror("VIDIOC_DQBUF");
		goto error;
	}
	
	if (ioctl(fd, VIDIOC_STREAMOFF, &buf.type) == -1) {
		perror("VIDIOC_STREAMOFF");
		goto error;
	}

	return buf.length;

error:
	munmap(*addr, buf.length);
	*addr = NULL;
	return -1;
}

int main(int argc, char *argv[])
{
	int camfd = 0;
	FILE *fp = NULL;
	int count, exitcode = 0, length;
	void *frame = NULL;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s device-file output-file\n", argv[0]);
		return EXIT_FAILURE;
	}

	camfd = open(argv[1], O_RDWR);

	if (camfd < 0) {
		perror("open");
		exitcode = EXIT_FAILURE;
		goto cleanup;
	}

	if (setformat(camfd, v4l2_fourcc('Y', 'U', 'Y', 'V')) < 0) {
		perror("setformat\n");
		exitcode = EXIT_FAILURE;
		goto cleanup;
	}

	count = reqbuffer(camfd);

	if (count < 0) {
		exitcode = EXIT_FAILURE;
		goto cleanup;
	} else if (count == 0) {
		fprintf(stderr, "No frames available\n");
		exitcode = EXIT_FAILURE;
		goto cleanup;
	}

	length = readframe(camfd, &frame);

	if (length < 0) {
		exitcode = EXIT_FAILURE;
		goto cleanup;
	} else if (length == 0) {
		fprintf(stderr, "No frames available\n");
		exitcode = EXIT_FAILURE;
		goto cleanup;
	}

	fp = fopen(argv[2], "w");

	if (fp == NULL) {
		perror("fopen");
		exitcode = EXIT_FAILURE;
		goto cleanup;
	}

	if (fwrite(frame, 1, length, fp) != length) {
		perror("fwrite");
		exitcode = EXIT_FAILURE;
	}

cleanup:
	if (fp != NULL)
		fclose(fp);
	if (camfd > 0)
		close(camfd);
	if (frame != NULL && length > 0)
		munmap(frame, length);

	return exitcode;
}
